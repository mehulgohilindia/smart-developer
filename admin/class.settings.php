<?php
/**
 * Settings API for SmartDeveloper
 *
 * @since 1.0
 */

if ( !class_exists('SmartDeveloper_Admin_Settings' ) ):

class SmartDeveloper_Admin_Settings {

    private $settings_api;

    function __construct() {

        $this->settings_api = new WP_Smart_Settings_API;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );

    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }

    function admin_menu() {
        add_options_page( __('Smart Developer','smartdev'), __('Smart Developer','smartdev'), 'manage_options', 'smart_developer_settings_panel', array($this, 'smart_developer_settings_page') );
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => 'general',
                'title' => __( 'General', 'smartdev' )
            ),
            array(
                'id'    => 'advanced',
                'title' => __( 'Advanced', 'smartdev' )
            )
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            'general' => array(
                array(
                    'name'              => 'text_val',
                    'label'             => __( 'Text Input', 'smartdev' ),
                    'desc'              => __( 'Text input description', 'smartdev' ),
                    'placeholder'       => __( 'Text Input placeholder', 'smartdev' ),
                    'type'              => 'text',
                    'default'           => 'Title',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'number_input',
                    'label'             => __( 'Number Input', 'smartdev' ),
                    'desc'              => __( 'Number field with validation callback `floatval`', 'smartdev' ),
                    'placeholder'       => __( '1.99', 'smartdev' ),
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '0.01',
                    'type'              => 'number',
                    'default'           => 'Title',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'        => 'textarea',
                    'label'       => __( 'Textarea Input', 'smartdev' ),
                    'desc'        => __( 'Textarea description', 'smartdev' ),
                    'placeholder' => __( 'Textarea placeholder', 'smartdev' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'html',
                    'desc'        => __( 'HTML area description. You can use any <strong>bold</strong> or other HTML elements.', 'smartdev' ),
                    'type'        => 'html'
                ),
                array(
                    'name'  => 'checkbox',
                    'label' => __( 'Checkbox', 'smartdev' ),
                    'desc'  => __( 'Checkbox Label', 'smartdev' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'radio',
                    'label'   => __( 'Radio Button', 'smartdev' ),
                    'desc'    => __( 'A radio button', 'smartdev' ),
                    'type'    => 'radio',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'selectbox',
                    'label'   => __( 'A Dropdown', 'smartdev' ),
                    'desc'    => __( 'Dropdown description', 'smartdev' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'smartdev' ),
                    'desc'    => __( 'Password description', 'smartdev' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'file',
                    'label'   => __( 'File', 'smartdev' ),
                    'desc'    => __( 'File description', 'smartdev' ),
                    'type'    => 'file',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose Image'
                    )
                )
            ),
            'advanced' => array(
                array(
                    'name'    => 'color',
                    'label'   => __( 'Color', 'smartdev' ),
                    'desc'    => __( 'Color description', 'smartdev' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'smartdev' ),
                    'desc'    => __( 'Password description', 'smartdev' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'wysiwyg',
                    'label'   => __( 'Advanced Editor', 'smartdev' ),
                    'desc'    => __( 'WP_Editor description', 'smartdev' ),
                    'type'    => 'wysiwyg',
                    'default' => ''
                ),
                array(
                    'name'    => 'multicheck',
                    'label'   => __( 'Multile checkbox', 'smartdev' ),
                    'desc'    => __( 'Multi checkbox description', 'smartdev' ),
                    'type'    => 'multicheck',
                    'default' => array('one' => 'one', 'four' => 'four'),
                    'options' => array(
                        'one'   => 'One',
                        'two'   => 'Two',
                        'three' => 'Three',
                        'four'  => 'Four'
                    )
                ),
            )
        );

        return $settings_fields;
    }

    function smart_developer_settings_page() {

        echo '<div class="wrap">';
        echo '<h1>' . __('Smart Developer Settings Panel', 'smartdev') . '</h1>';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';

    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }

}
endif;
