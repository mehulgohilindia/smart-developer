<?php
/**
 *  Plugin Name: Smart Developer
 *  Version: 1.0.0
 *  Plugin URI:  https://www.mehulgohil.in/plugins/smart-developer/
 *  Description: This WordPress plugin will help developers to cut down the time of development in many different ways.
 *  Author:      Mehul Gohil
 *  Author URI:  https://wpdazzling.com/
 *  License:     GPLv3
 *  License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 *  Text Domain: smartdev
 *  Domain Path: /languages
 *
 */

// Exit if accessed directly.
if ( ! defined( 'WPINC' ) ) {
	exit;
}

if ( ! class_exists( 'SmartDeveloper' ) ) :
	/**
	 * Main SmartDeveloper Class
	 *
	 * @since 1.0
	 */
	final class SmartDeveloper {
		/**
		 * SmartDeveloper Instance
		 *
		 * @since  1.0
		 * @access private
		 *
		 * @var    SmartDeveloper
		 */
		private static $instance;
		/**
		 * SmartDeveloper Settings Object
		 *
		 * @since  1.0
		 * @access public
		 *
		 * @var    SmartDeveloper Settings object
		 */
		public $settings;
		/**
		 * SmartDeveloper Session Object
		 *
		 * This holds donation data for user's session.
		 *
		 * @since  1.0
		 * @access public
		 *
		 * @var    SmartDeveloper Session object
		 */
		public $session;
		/**
		 * Main SmartDeveloper Instance
		 *
		 * Insures that only one instance of SmartDeveloper exists in memory. Also prevents needing to define globals all over the place.
		 *
		 * @since     1.0
		 * @access    public
		 *
		 * @static
		 * @staticvar array $instance
		 * @uses      SmartDeveloper::setup_constants() Setup the constants needed.
		 * @uses      SmartDeveloper::includes() Include the required files.
		 * @uses      SmartDeveloper::load_textdomain() load the language files.
		 * @see       SmartDeveloper()
		 *
		 * @return    SmartDeveloper
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof SmartDeveloper ) ) {
				self::$instance = new SmartDeveloper;
				self::$instance->setup_constants();
				add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
				self::$instance->includes();
                self::$instance->settings        = new SmartDeveloper_Admin_Settings();
				//self::$instance->session         = new SmartDeveloper_Session();
				/**
				 * Fire the action after SmartDeveloper core loads
				 *
				 * @since 1.0.0
				 */
				do_action( 'smartdev_init', self::$instance );
			}
			return self::$instance;
		}
		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is a single
		 * object, therefore we don't want the object to be cloned.
		 *
		 * @since  1.0
		 * @access protected
		 *
		 * @return void
		 */
		public function __clone() {
			// Cloning instances of the class is forbidden
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'smartdev' ), '1.0' );
		}
		/**
		 * Disable unserializing of the class
		 *
		 * @since  1.0
		 * @access protected
		 *
		 * @return void
		 */
		public function __wakeup() {
			// Unserializing instances of the class is forbidden.
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'smartdev' ), '1.0' );
		}
		/**
		 * Setup plugin constants
		 *
		 * @since  1.0
		 * @access private
		 *
		 * @return void
		 */
		private function setup_constants() {
			// Plugin version
			if ( ! defined( 'SMART_DEVELOPER_VERSION' ) ) {
				define( 'SMART_DEVELOPER_VERSION', '1.0.0' );
			}
			// Plugin Folder Path
			if ( ! defined( 'SMART_DEVELOPER_PLUGIN_DIR' ) ) {
				define( 'SMART_DEVELOPER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}
			// Plugin Folder URL
			if ( ! defined( 'SMART_DEVELOPER_PLUGIN_URL' ) ) {
				define( 'SMART_DEVELOPER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}
			// Plugin Basename aka: "smart-developer/smart-developer.php"
			if ( ! defined( 'SMART_DEVELOPER_PLUGIN_BASENAME' ) ) {
				define( 'SMART_DEVELOPER_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
			}
			// Plugin Root File
			if ( ! defined( 'SMART_DEVELOPER_PLUGIN_FILE' ) ) {
				define( 'SMART_DEVELOPER_PLUGIN_FILE', __FILE__ );
			}
		}
		/**
		 * Include required files
		 *
		 * @since  1.0
		 * @access private
		 *
		 * @return void
		 */
		private function includes() {

			require_once SMART_DEVELOPER_PLUGIN_DIR . 'install.php';

            // Call these files in case of admin access only.
            if(is_admin()) {

                require_once SMART_DEVELOPER_PLUGIN_DIR . 'admin/class.settings.php';
                require_once SMART_DEVELOPER_PLUGIN_DIR . 'admin/class.settings-api.php';
            }

            // Call for admin notices
            require_once SMART_DEVELOPER_PLUGIN_DIR . 'includes/notices.php';

		}
		/**
		 * Loads the plugin language files
		 *
		 * @since  1.0
		 * @access public
		 *
		 * @return void
		 */
		public function load_textdomain() {

            // Set filter for languages directory
			$smartdev_lang_dir = dirname( plugin_basename( SMART_DEVELOPER_PLUGIN_FILE ) ) . '/languages/';
			$smartdev_lang_dir = apply_filters( 'smartdev_languages_directory', $smartdev_lang_dir );

            // Traditional WordPress plugin locale filter
			$locale = apply_filters( 'plugin_locale', get_locale(), 'smartdev' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'smartdev', $locale );

            // Setup paths to current locale file
			$mofile_local  = $smartdev_lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/smartdev/' . $mofile;

            if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/smartdev folder
				load_textdomain( 'smartdev', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local location from filter `smartdev_languages_directory`
				load_textdomain( 'smartdev', $mofile_local );
			} else {
				// Load the default language files packaged up w/ Smart Developer Plugin
				load_plugin_textdomain( 'smartdev', false, $smartdev_lang_dir );
			}

		}
	}
endif; // End if class_exists check

/**
 * Initialize Smart Developer
 *
 * The main function responsible for returning the SmartDeveloper instance for functioning.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $smartdev = SmartDeveloper(); ?>
 *
 * @since 1.0
 * @return object|SmartDeveloper
 */
function SmartDeveloper() {
	return SmartDeveloper::instance();
}
// Get SmartDeveloper Running
SmartDeveloper();
