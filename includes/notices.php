<?php
function smartdev_admin_notice(){

    echo '<div class="notice notice-success is-dismissible">
             <p>Hey Smarty, Thanks for trying Smart Developer plugin.</p>
         </div>';

}
add_action('admin_notices', 'smartdev_admin_notice');
